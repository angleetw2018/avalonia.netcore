﻿using Avalonia.Controls;
using Models;
using SqlSugar;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace AvaloniaApplication1.ViewModels;

public class MainViewModel : ViewModelBase
{
    public ObservableCollection<COMPANY> Companies { get; }
    ConnectionConfig connectionConfig = new ConnectionConfig();
 

    public MainViewModel()
    {
        connectionConfig.DbType = DbType.Sqlite;
        connectionConfig.ConnectionString = Setting.connStr;


        using (SqlSugarClient sugarClient = new SqlSugarClient(connectionConfig))
        {
            sugarClient.DbFirst.CreateClassFile(Setting.classStr);
            var companies = sugarClient.Queryable<COMPANY>().Select(x => new COMPANY
            {
                ID = x.ID,
                SALARY = x.SALARY,
                SELECTED = false,
                ADDRESS = x.ADDRESS,
                AGE = x.AGE,
                NAME = x.NAME,
            }).ToList();

            Companies = new ObservableCollection<COMPANY>(companies);
        }
    }

   
}