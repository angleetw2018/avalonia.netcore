﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
   public partial class COMPANY
    {
        [SugarColumn(IsIgnore = true)]
        public bool SELECTED { get; set; }
    }
}
