﻿using Avalonia.Controls;
using Avalonia.Interactivity;
using AvaloniaApplication1.ViewModels;
using Models;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace AvaloniaApplication1.Views;

public partial class MainView : UserControl
{
    ConnectionConfig connectionConfig = new ConnectionConfig();
   
    public MainView()
    {
        connectionConfig.DbType = DbType.Sqlite;
        connectionConfig.IsAutoCloseConnection = true;
        connectionConfig.ConnectionString = Setting.connStr;
        InitializeComponent();
    }


    private void Search(object sender, RoutedEventArgs args)
    {
        using (SqlSugarClient sugarClient = new SqlSugarClient(connectionConfig))
        {
            var companies = sugarClient.Queryable<COMPANY>().Select(x => new COMPANY
            {
                ID = x.ID,
                SALARY = x.SALARY,
                SELECTED = false,
                ADDRESS = x.ADDRESS,
                AGE = x.AGE,
                NAME = x.NAME,
            }).ToList();

            datagrid.ItemsSource = new ObservableCollection<COMPANY>(companies);
        }
    }
    private void Add(object sender, RoutedEventArgs args)
    {
        var datasource = datagrid.ItemsSource as ObservableCollection<COMPANY>;
        if(datasource != null)
        {
            using (SqlSugarClient sugarClient = new SqlSugarClient(connectionConfig))
            {
                var data = datasource.ToList();
                var id = sugarClient.Queryable<COMPANY>()
                    .Select(x => SqlFunc.AggregateMax(x.ID)).First();
                datasource.Add(new COMPANY() { ID = id + 1 });

            }
        }
    }
    private void Save(object sender, RoutedEventArgs args)
    {
        var datasource = datagrid.ItemsSource as ObservableCollection<COMPANY>;
        if (datasource != null)
        {
            using (SqlSugarClient sugarClient = new SqlSugarClient(connectionConfig))
            {
                var list = datasource.ToList();
                sugarClient.Storageable<COMPANY>(list)
                    .WhereColumns(it => it.ID)
                    .ExecuteCommand();

            }
        }
    }

    private void Delete(object sender, RoutedEventArgs args)
    {
        var datasource = datagrid.ItemsSource as ObservableCollection<COMPANY>;
        if(datasource != null)
        {
            var data = datasource[datagrid.SelectedIndex];

            using (SqlSugarClient sugarClient = new SqlSugarClient(connectionConfig))
            {
                sugarClient.Deleteable<COMPANY>()
                    .Where(x=>x.ID == data.ID)
                    .ExecuteCommand();
                datasource.RemoveAt(datagrid.SelectedIndex);
            }
        }

    }


}
